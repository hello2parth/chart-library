# D3 Charts
Custom common library for Line, LineBar and Gauge chart based on D3.js. Provides different basic configuration options based on type of chart.

## Features
* Responsive
* Customizable
* Light-weight
* Single bundled file for all three chart

## Type of charts
* Line chart
* LineBar chart
* Gauge chart

### Prerequisties
Requires D3.js to be included

```````
<script src="d3/d3.v3.min.js"></script>
```````

## Run
Run simply with
````
npm start
````
