import './style.css';

export function getRandomInt (min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

var aData = [
  { "name": "Activity 1", "todayvalue": 6000, "monthvalue": 8000 },
  { "name": "Activity 2", "todayvalue": 2, "monthvalue": 0 },
  { "name": "Activity 3", "todayvalue": 4595, "monthvalue": 1289 },
  { "name": "Activity 4", "todayvalue": 455, "monthvalue": 1200 },
  { "name": "Activity 5", "todayvalue": 1965, "monthvalue": 1200 },
  { "name": "Activity 6", "todayvalue": 1599, "monthvalue": 2000 },
  { "name": "Activity 7", "todayvalue": 1, "monthvalue": 5 },
  { "name": "Activity 8", "todayvalue": 148575, "monthvalue": 160000 },
  { "name": "Activity 9", "todayvalue": 479, "monthvalue": 300 },
  { "name": "Activity 10", "todayvalue": 29744, "monthvalue": 28129 },
  { "name": "Activity 11", "todayvalue": 1, "monthvalue": 3 },
  { "name": "Activity 12", "todayvalue": 1530, "monthvalue": 1340 },
  { "name": "Activity 13", "todayvalue": 18401, "monthvalue": 17408 }
];
var oBarChart = new Chart.init({
  id: "#BarChart",
  type: "Bar",
  title: "Count - Today",
  dimension: "name",
  measure: "todayvalue",
  legend: "Today"
});
oBarChart.update(aData);

//Data update experiments
export function onBCDataSet1Click() {
  oBarChart.update(aDataSet1);
}
export function onBCDataSet2Click() {
  oBarChart.update(aDataSet2);
}
export function onBCDataSet3Click() {
  oBarChart.update(aDataSet3);
}
export function onBCDataSetRemove() {
  oBarChart.update([]);
}

var oLineBarChart = new Chart.init({
  id: "#LineBarChart",
  type: "LineBar",
  title: "Count",
  dimension: "name",
  barMeasure: "todayvalue",
  lineMeasure: "monthvalue",
  legend: {
    line: "Today",
    bar: "Monthly average"
  }
});

oLineBarChart.update(aData);

//Data update experiments
export function onLBCDataSet1Click() {
  oLineBarChart.update(aDataSet1);
}
export function onLBCDataSet2Click() {
  oLineBarChart.update(aDataSet2);
}
export function onLBCDataSet3Click() {
  oLineBarChart.update(aDataSet3);
}

export function onLBCDataSetRemove() {
  oLineBarChart.update([]);
}

var oGaugeChart = new Chart.init({
  id: "#Gauge",
  type: "Gauge",
  value: 60,
  units: "um",
  responsive: true,
  minRange: 0,
  maxRange: 180
});

export function onUpdateGCClick() {
  var oGCInput = document.getElementById("GCDataValue");
  let range = parseInt(oGCInput.value, 10);
  range = range > 180 ? 180 : range < 0 ? 0: range;
  oGaugeChart.setValue(range);
  oGCInput.value = range;
}

export function onGCRangeUpdateClick() {
  oGaugeChart.updateRange( parseInt(document.getElementById("GCRangeStart").value, 10), parseInt(document.getElementById("GCRangeEnd").value, 10));
}
var aDataSet1 = [
  { "name": "Activity 1", "todayvalue": 6000, "monthvalue": 8000 },
  { "name": "Activity 2", "todayvalue": 2, "monthvalue": 0 },
  { "name": "Activity 3", "todayvalue": 4595, "monthvalue": 1289 },
  { "name": "Activity 4", "todayvalue": 455, "monthvalue": 1200 },
  { "name": "Activity 5", "todayvalue": 1965, "monthvalue": 1200 },
  { "name": "Activity 6", "todayvalue": 1599, "monthvalue": 2000 },
  { "name": "Activity 7", "todayvalue": 1, "monthvalue": 5 },
  { "name": "Activity 8", "todayvalue": 148575, "monthvalue": 160000 },
  { "name": "Activity 9", "todayvalue": 479, "monthvalue": 300 },
  { "name": "Activity 10", "todayvalue": 29744, "monthvalue": 28129 },
  { "name": "Activity 11", "todayvalue": 1, "monthvalue": 3 },
  { "name": "Activity 12", "todayvalue": 1530, "monthvalue": 1340 },
  { "name": "Activity 13", "todayvalue": 18401, "monthvalue": 17408 },
  { "name": "Activity 14", "todayvalue": 28401, "monthvalue": 47408 }
];

var aDataSet2 = [
  { "name": "Activity 1", "todayvalue": 6000, "monthvalue": 8000 },
  { "name": "Activity 2", "todayvalue": 2, "monthvalue": 0 },
  { "name": "Activity 3", "todayvalue": 4595, "monthvalue": 1289 },
  { "name": "Activity 4", "todayvalue": 455, "monthvalue": 1200 },
  { "name": "Activity 5", "todayvalue": 1965, "monthvalue": 1200 },
  { "name": "Activity 6", "todayvalue": 1599, "monthvalue": 2000 },
  { "name": "Activity 7", "todayvalue": 1, "monthvalue": 5 },
  { "name": "Activity 8", "todayvalue": 148575, "monthvalue": 160000 },
  { "name": "Activity 9", "todayvalue": 479, "monthvalue": 300 },
  { "name": "Activity 10", "todayvalue": 29744, "monthvalue": 28129 },
  { "name": "Activity 11", "todayvalue": 1, "monthvalue": 3 }
];
var aDataSet3 = [
  { "name": "Activity 1", "todayvalue": 6000, "monthvalue": 8000 },
  { "name": "Activity 2", "todayvalue": 2, "monthvalue": 0 },
  { "name": "Activity 3", "todayvalue": 1965, "monthvalue": 1200 },
  { "name": "Activity 4", "todayvalue": 1599, "monthvalue": 2000 },
  { "name": "Activity 5", "todayvalue": 1, "monthvalue": 5 },
  { "name": "Activity 6", "todayvalue": 148575, "monthvalue": 160000 },
  { "name": "Activity 7", "todayvalue": 479, "monthvalue": 300 },
  { "name": "Activity 8", "todayvalue": 29744, "monthvalue": 28129 },
  { "name": "Activity 9", "todayvalue": 1, "monthvalue": 3 },
  { "name": "Activity 10", "todayvalue": 1530, "monthvalue": 1340 },
  { "name": "Activity 11", "todayvalue": 18401, "monthvalue": 17408 },
  { "name": "Activity 12", "todayvalue": 121000, "monthvalue": 150000 }
];
