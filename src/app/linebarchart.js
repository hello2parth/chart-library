import util from "./util";
import "./css/LineBarChart.css";
import * as d3 from './d3/d3.v3.min.js';

function resize() {
    var _this = this;
    this.width = parseInt(d3.select(this.options.id).style("width"), 10) - this.margin.left - this.margin.right;
    this.height = parseInt(d3.select(this.options.id).style("height"), 10) - this.margin.top - this.margin.bottom;

    this.x.rangeRoundBands([0, this.width], .1, .3);

    this.y.range([this.height, 20]);

    d3.select(this.options.id + " svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom);

    this.svg.select(".y.axis").remove();
    this.svg.select(".x.axis").remove();

    this.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + this.height + ")")
        .call(this.xAxis)
        .selectAll(".tick text")
        .call(util.wrap, this.x.rangeBand());

    this.svg.append("g")
        .attr("class", "y axis")
        .call(this.yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(this.options.title);

    this.svg.selectAll(".bar")
        .transition()
        .duration(450)
        .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
        .attr("y", function (d) { return _this.y(d[_this.options.barMeasure]); })
        .attr("height", function (d) { return _this.height - _this.y(d[_this.options.barMeasure]); })
        .attr("width", this.x.rangeBand());

    this.svg.selectAll(".line")
        .transition()
        .duration(450)
        .attr("d", this.line)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round");


    this.svg.selectAll(".legend-rect")
        .attr("x", function(d, i) { return _this.x.rangeExtent()[1] - 400 + i * 250; })
         .attr("y", function(d, i) { return  5; })
         .attr("class", function(d){ return "legend-rect " + d.class; });

    this.svg.selectAll(".legend-label")
        .attr("x", function(d, i) { return (_this.x.rangeExtent()[1] - 400) + (i * 250) + 15;							})
        .attr("y", function(d, i) { return  15; })
        .text(function(d) { return d.label; });
}

 function chart(args, _this) {
    //global references
    this.svg = null;

    //default options
    var defaults = {
        id: "#chart",
        className: "jl-linebar",
        title: "",
        dimension: "name",
        barMeasure: "value1",
        lineMeasure: "value2",
        responsive: true,
        legend: {
            line: "",
            bar: ""
        }
    };

    if (args, _this) {
        if (typeof args === "string") {
            this.options = util.extendDefaults(defaults, {
                id: args
            });
        }
        else if (typeof args === "object") {
            _this.options = util.extendDefaults(defaults, args);
        }
        //Create Chart
        _this.margin = { top: 20, right: 20, bottom: 50, left: 50 };
        _this.width = parseInt(d3.select(_this.options.id).style("width"), 10) - _this.margin.left - _this.margin.right;
        _this.height = parseInt(d3.select(_this.options.id).style("height"), 10) - _this.margin.top - _this.margin.bottom;

        _this.x = d3.scale.ordinal()
            .rangeRoundBands([0, _this.width], .1, .3);

        _this.y = d3.scale.linear()
            .range([_this.height, 20]);

        _this.xAxis = d3.svg.axis()
            .scale(_this.x)
            .orient("bottom");

        _this.yAxis = d3.svg.axis()
            .scale(_this.y)
            .orient("left")
            .tickFormat(function (d) {
                //return d.toLocaleString();
                return d > 1000 ? (d / 1000) + "k" : d;
            });

        var _this = _this;
        _this.line = d3.svg.line()
            .x(function (d) {
                return _this.x(d[_this.options.dimension]) + _this.x.rangeBand() / 2;
            })
            .y(function (d) {
                return _this.y(d[_this.options.lineMeasure]);
            })
            .interpolate("monotone")
            .tension(0.9);

        _this.svg = d3.select(_this.options.id).append("svg")
            .attr("class", _this.options.className)
            .attr("width", _this.width + _this.margin.left + _this.margin.right)
            .attr("height", _this.height + _this.margin.top + _this.margin.bottom)
            .append("g")
            .attr("transform", "translate(" + _this.margin.left + "," + _this.margin.top + ")");

        _this.legend = _this.svg.append("g").attr("class", "legend");

    }
}

function chartUpdate(data, _this) {
    var iBarMax = d3.max(data, function (d) { return d[_this.options.barMeasure];}),
        iLineMax = d3.max(data, function (d) { return d[_this.options.lineMeasure];});

    _this.x.domain(data.map(function (d) { return d[_this.options.dimension]; }));
    _this.y.domain([0, (iBarMax > iLineMax ? iBarMax : iLineMax)]);

    _this.svg.select(".y.axis").remove();
    _this.svg.select(".x.axis").remove();

    _this.svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + _this.height + ")")
        .call(_this.xAxis)
        .selectAll(".tick text")
        .call(util.wrap, _this.x.rangeBand());

    _this.svg.append("g")
        .attr("class", "y axis")
        .call(_this.yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(_this.options.title);

    var bar = _this.svg.selectAll(".bar")
        .data(data, function (d) { return d[_this.options.dimension]; });

    // new data: place bars
    bar.enter().append("rect")
        .attr("class", "bar")
        .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
        .attr("y", function (d) { return _this.y(d[_this.options.barMeasure]); })
        .attr("height", function (d) {
            return _this.height - _this.y(d[_this.options.barMeasure]);
        })
        .attr("width", _this.x.rangeBand());

    // removed data: remove bars
    bar.exit().remove();

    // updated data: update bars
    bar.transition()
        .duration(750)
        .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
        .attr("y", function (d) { return _this.y(d[_this.options.barMeasure]); })
        .attr("height", function (d) { return _this.height - _this.y(d[_this.options.barMeasure]); });


    var linepath = _this.svg.selectAll(".line").data([data]);

    linepath.enter().append("path")
        .attr("d", _this.line)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke-linejoin", "round")
        .attr("stroke-linecap", "round");

    linepath.transition()
        .duration(750)
        .attr("d", _this.line);

    linepath.exit().remove();

    //Legend handlers
    var legendData = [];
    if(_this.options.legend.line && _this.options.legend.bar){
        legendData = [
            { label: _this.options.legend.line, class:"legend-bar"},
            { label: _this.options.legend.bar, class:"legend-line"}
        ];
    }

    // add legend
    var legendRect = _this.legend.selectAll('.legend-rect').data(legendData);

    legendRect.enter().append("rect")
        .attr("width", 10)
        .attr("height", 10)
        .attr("x", function(d, i) { return _this.x.rangeExtent()[1] - 400 + i * 250; })
         .attr("y", function(d, i) { return  5; })
         .attr("class", function(d){ return "legend-rect " + d.class; });

    var legendText = _this.legend.selectAll('.legend-label').data(legendData);

    legendText.enter().append("text")
        .attr("class","legend-label")
        .attr("x", function(d, i) { return (_this.x.rangeExtent()[1] - 400) + (i * 250) + 15;							})
        .attr("y", function(d, i) { return  15; })
        .text(function(d) { return d.label; });
}

export default {resize, chart, chartUpdate};
