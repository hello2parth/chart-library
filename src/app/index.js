import util from "./util";
import BarChart from "./barchart";
import LineBarChart from "./linebarchart";
import Gauge from "./gauge";
class init {
  constructor(args) {
    var _this = this;
    this.args = args;
    if (args.type === "Bar") {
      BarChart.chart(args, _this);
    }
    else if (args.type === "LineBar") {
      LineBarChart.chart(args, _this);
    }
    else if (args.type === "Gauge") {
      Gauge.chart(args, _this);
      this.render();
      this.update();
    }
    util.initializeEvents.call(_this);
  }
  update(data) {
    var _this = this;
    if (this.args.type === "Bar") {
      BarChart.chartUpdate(data, _this);
    }
    else if (this.args.type === "LineBar") {
      LineBarChart.chartUpdate(data, _this);
    }
    else if (this.args.type === "Gauge") {
      Gauge.chartUpdate(_this);
    }
  }
  render() {
    if (this.args.type === "Gauge") {
        var _this = this;
        Gauge.render(_this);
    }
  }
  setValue(v){
    if (this.args.type === "Gauge") {
        this.options.value = v;
        this.update();
    }
  }
  updateRange(min, max){
    if (this.args.type === "Gauge") {
      if(min < max){
        this.options.minRange = min;
        this.options.maxRange = max;
        this.update();
      }
      else {
        //show range error
      }
    }
  }
  getValue(){
    if (this.args.type === "Gauge") {
        return this.options.value;
    }
  }
  remove() {
    this.svg.remove();
    d3.select(this.options.id + "svg").remove();
  }
}

export {init}; // var oChart = new Chart.init{(args)};
//export default init; // var oChart = new Chart.default{(args)};
//export default {init}; // var oGauge = new Chart.default.init{(args)};
