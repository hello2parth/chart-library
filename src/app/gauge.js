import util from "./util";
import "./css/Gauge.css";
import * as d3 from './d3/d3.v3.min.js';

var myScale =  d3.scale.linear();
//Private Methods
var scaledPerc = function(min, max, value) {
    myScale.domain([min, max]).range([0, 100]);
    return Math.round(myScale(value) * 100)/100;
}
var percToDeg = function (perc) {
    return perc * 360;
};

var degToRad = function (deg) {
    return deg * Math.PI / 180;
};

var percToRad = function (perc) {
    return degToRad(percToDeg(perc));
};

var repaintGauge = function (perc) {
    var nextStart = this.totalPercent;
    var arcStartRad = percToRad(nextStart);
    var arcEndRad = arcStartRad + percToRad(perc / 2);
        nextStart += perc / 2;

    this.arc1.startAngle(arcStartRad).endAngle(arcEndRad);

        arcStartRad = percToRad(nextStart);
        arcEndRad = arcStartRad + percToRad((1 - perc) / 2);

    this.arc2.startAngle(arcStartRad + this.padRad).endAngle(arcEndRad);
    this.chart.select(".chart-filled").attr('d', this.arc1);
    this.chart.select(".chart-empty").attr('d', this.arc2);
};

var recalcPointerPos = function (perc) {
    var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
    thetaRad = percToRad(perc / 2);
    centerX = 0;
    centerY = 0;
    topX = centerX - this.len * Math.cos(thetaRad);
    topY = centerY - this.len * Math.sin(thetaRad);
    leftX = centerX - this.needleradius * Math.cos(thetaRad - Math.PI / 2);
    leftY = centerY - this.needleradius * Math.sin(thetaRad - Math.PI / 2);
    rightX = centerX - this.needleradius * Math.cos(thetaRad + Math.PI / 2);
    rightY = centerY - this.needleradius * Math.sin(thetaRad + Math.PI / 2);
    return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
};

function resize() {
    var _this = this;

    this.width = parseInt(d3.select(this.options.id).style("width"), 10) - this.margin.left - this.margin.right;
    this.height = parseInt(d3.select(this.options.id).style("height"), 10) - this.margin.top - this.margin.bottom;

    this.radius = Math.min(this.width, this.height) / 2;
    this.barWidth = 4 * this.width / 3;
    this.len = this.radius * 0.9;
    this.needleradius = this.len / 9;
    this.fontSize = Math.round(this.width / (this.fontRatio * 2));

    d3.select(this.options.id + " svg")
        .attr("width", this.width + this.margin.left + this.margin.right)
        .attr("height", this.height + this.margin.top + this.margin.bottom);

    this.chart.attr('transform', "translate(" + ((this.width + this.margin.left) / 2) + ", " + ((this.height + this.margin.top) / 2) + ")");
    this.label.attr('transform', "translate(" + ((this.width + this.margin.left) / 2) + ", " + ((this.height + this.margin.top) / 2) + ")");


    this.arc1 = d3.svg.arc().outerRadius(this.radius - this.chartInset).innerRadius(this.radius - this.chartInset - this.barWidth);
    this.arc2 = d3.svg.arc().outerRadius(this.radius - this.chartInset).innerRadius(this.radius - this.chartInset - this.barWidth);


    this.labelText.attr("dy", function () { return _this.needleradius + _this.fontSize;})
        .style("font-size", function(){ return _this.fontSize + "px"; });

    this.chart.select('.needle-center').attr('r', this.needleradius);
    this.chart.select('.needle').attr('d', recalcPointerPos.call(this, 0));

    chartUpdate.call(this, _this);
}

function chart(args, _this) {
    //global references
    this.svg = null;

    //default options
    var defaults = {
        id: "#chart",
        className: "jl-gauge",
        title: "",
        responsive: true,
        units: "",
        value: 0,
        minRange: 0,
        maxRange: 180
    };

    if (args) {
        if (typeof args === "string") {
            _this.options = util.extendDefaults(defaults, {
                id: args
            });
        }
        else if (typeof args === "object") {
            _this.options = util.extendDefaults(defaults, args);
        }

        //Create Chart
        _this.sContainer = args;
        _this.margin = { top: 10, right: 10, bottom: 10, left: 10 };
        _this.width = parseInt(d3.select(_this.options.id).style("width"), 10) - _this.margin.left - _this.margin.right;
        _this.height = parseInt(d3.select(_this.options.id).style("height"), 10) - _this.margin.top - _this.margin.bottom;

        _this.radius = Math.min(_this.width, _this.height) / 2;
        _this.barWidth = 4 * _this.width / 3;
        _this.padRad = 0.025;
        _this.chartInset = 10;
        _this.len = _this.radius * 0.9 ;
        _this.needleradius = _this.len / 9;
        _this.totalPercent = 0.75;
        _this.fontRatio = 7.6;
        _this.fontSize = Math.round(_this.width / (_this.fontRatio * 2 )); //Needs to be improved.

        _this.el = d3.select(_this.options.id);

        _this.svg = _this.el.append("svg")
            .attr("class", _this.options.className)
            .attr("width", _this.width + _this.margin.left + _this.margin.right)
            .attr("height", _this.height + _this.margin.top + _this.margin.bottom);

        _this.chart = _this.svg.append('g')
            .attr('transform', "translate(" + ((_this.width + _this.margin.left) / 2) + ", " + ((_this.height + _this.margin.top) / 2) + ")");
        _this.label = _this.svg.append('g')
            .attr('transform', "translate(" + ((_this.width + _this.margin.left) / 2) + ", " + ((_this.height + _this.margin.top) / 2) + ")");
        _this.chart.append('path')
            .attr('class', "arc chart-filled");
        _this.chart.append('path')
            .attr('class', "arc chart-empty");

        _this.arc1 = d3.svg.arc().outerRadius(_this.radius - _this.chartInset).innerRadius(_this.radius - _this.chartInset - _this.barWidth);
        _this.arc2 = d3.svg.arc().outerRadius(_this.radius - _this.chartInset).innerRadius(_this.radius - _this.chartInset - _this.barWidth);
        return _this;
    }
}

function chartUpdate(_this) {
    var oldValue = _this.perc || 0;
    _this.rangeConVal = scaledPerc(_this.options.minRange , _this.options.maxRange, _this.options.value);
    if(_this.rangeConVal <  0 ){
        _this.rangeConVal = 0;
    }
    else if(_this.rangeConVal > 100){
        _this.rangeConVal = 100;
    }
    _this.perc = _this.rangeConVal / 100;
    //Set label
    _this.labelText.text(_this.options.value + _this.options.units);
    // Reset pointer position
    _this.chart.transition().delay(100).ease('quad').duration(200).select('.needle').tween('reset-progress', function () {
        return function (percentOfPercent) {
            var progress = (1 - percentOfPercent) * oldValue;
            repaintGauge.call(_this, progress);
            return d3.select(this).attr('d', recalcPointerPos.call(_this, progress));
        };
    });
    _this.chart.transition().delay(300).ease('bounce').duration(1500).select('.needle').tween('progress', function () {
        return function (percentOfPercent) {
            var progress = percentOfPercent * _this.perc;
            repaintGauge.call(_this, progress);
            return d3.select(this).attr('d', recalcPointerPos.call(_this, progress));
        };
    });
}

function render(_this) {
    _this.labelText = _this.label.append("text")
        .attr("dx", 0)
        .attr("dy", function () { return _this.needleradius + _this.fontSize; })
        .attr("text-anchor", "middle")
        .style("font-size", function(){ return _this.fontSize + "px"; })
        .text("");

    _this.chart.append('circle').attr('class', 'needle-center').attr('cx', 0).attr('cy', 0).attr('r', _this.needleradius);
    return _this.chart.append('path').attr('class', 'needle').attr('d', recalcPointerPos.call(_this, 0));
}
export default {resize, chart, chartUpdate, render};
