import util from "./util";
import "./css/BarChart.css";
import * as d3 from './d3/d3.v3.min.js';


    function resize() {
        var _this = this;
        this.width = parseInt(d3.select(this.options.id).style("width"), 10) - this.margin.left - this.margin.right;
        this.height = parseInt(d3.select(this.options.id).style("height"), 10) - this.margin.top - this.margin.bottom;

        this.x.rangeRoundBands([0, this.width], .1, .3);

        this.y.range([this.height, 0]);

        d3.select(this.options.id + " svg")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom);

        this.svg.select(".y.axis").remove();
        this.svg.select(".x.axis").remove();

        this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.height + ")")
            .call(this.xAxis)
            .selectAll(".tick text")
            .call(util.wrap, this.x.rangeBand());

        this.svg.append("g")
            .attr("class", "y axis")
            .call(this.yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(this.options.title);

        this.svg.selectAll(".bar")
            .transition()
            .duration(750)
            .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
            .attr("y", function (d) { return _this.y(d[_this.options.measure]); })
            .attr("height", function (d) { return _this.height - _this.y(d[_this.options.measure]); })
            .attr("width", this.x.rangeBand());
    }

    function chart(args, _this) {

        //global references
        _this.svg = null;

        //default options
        var defaults = {
            id: "#chart",
            className: "jl-bar",
            title: "",
            dimension: "name",
            measure: "value",
            responsive: true,
            legend: ""
        };

        if (args) {
            if (typeof args === "string") {
                _this.options = util.extendDefaults(defaults, {
                    id: args
                });
            }
            else if (typeof args === "object") {
                _this.options = util.extendDefaults(defaults, args);
            }

                //Create BarChart
                _this.sContainer = args;
                _this.margin = { top: 20, right: 20, bottom: 50, left: 50 };
                _this.width = parseInt(d3.select(_this.options.id).style("width"), 10) - _this.margin.left - _this.margin.right;
                _this.height = parseInt(d3.select(_this.options.id).style("height"), 10) - _this.margin.top - _this.margin.bottom;

                _this.x = d3.scale.ordinal()
                    .rangeRoundBands([0, _this.width], .1, .3);
                _this.y = d3.scale.linear()
                    .range([_this.height, 0]);
                _this.xAxis = d3.svg.axis()
                    .scale(_this.x)
                    .orient("bottom");
                _this.yAxis = d3.svg.axis()
                    .scale(_this.y)
                    .orient("left")
                    .tickFormat(function (d) {
                        //return d.toLocaleString();
                        return d > 1000 ? (d / 1000) + "k" : d;
                    });
                _this.svg = d3.select(_this.options.id).append("svg")
                    .attr("class", _this.options.className)
                    .attr("width", _this.width + _this.margin.left + _this.margin.right)
                    .attr("height", _this.height + _this.margin.top + _this.margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + _this.margin.left + "," + _this.margin.top + ")");

                    util.initializeEvents.call(_this);
        }
    }

    function chartUpdate(data, _this) {
            //update BarChart
            _this.x.domain(data.map(function (d) { return d[_this.options.dimension]; }));
            _this.y.domain([0, d3.max(data, function (d) { return d[_this.options.measure]; })]);

            _this.svg.select(".y.axis").remove();
            _this.svg.select(".x.axis").remove();

            _this.svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + _this.height + ")")
                .call(_this.xAxis)
                .selectAll(".tick text")
                .call(util.wrap, _this.x.rangeBand());

            _this.svg.append("g")
                .attr("class", "y axis")
                .call(_this.yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text(_this.options.title);

            var bar = _this.svg.selectAll(".bar")
                .data(data, function (d) { return d[_this.options.dimension]; });

            // new data:
            bar.enter().append("rect")
                .attr("class", "bar")
                .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
                .attr("y", function (d) { return _this.y(d[_this.options.measure]); })
                .attr("height", function (d) {
                    return _this.height - _this.y(d[_this.options.measure]);
                })
                .attr("width", _this.x.rangeBand());

            // removed data:
            bar.exit().remove();
            // label.exit().remove();

            // updated data:
            bar.transition()
                .duration(750)
                .attr("x", function (d) { return _this.x(d[_this.options.dimension]); })
                .attr("y", function (d) { return _this.y(d[_this.options.measure]); })
                .attr("height", function (d) { return _this.height - _this.y(d[_this.options.measure]); });
    }
export default {resize, chart, chartUpdate};
