import BarChart from "./barchart";
import LineBarChart from "./linebarchart";
import Gauge from "./gauge";

function extendDefaults(source, properties) {
    var property;
    for (property in properties) {
        if (properties.hasOwnProperty(property)) {
            source[property] = properties[property];
        }
    }
    return source;
}

function wrap(text, width) {
    text.each(function () {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 0.9, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
    });
}

function initializeEvents() {
    var _this = this;
    if (this.options.responsive) {
        this.resizeHandler = d3.select(window).on("resize." + this.options.id.replace(/[.#]/g, ''), function () {
            if (_this.args.type === "Bar") {
                BarChart.resize.call(_this);
              }
            else if (_this.args.type === "LineBar") {           
                LineBarChart.resize.call(_this);
            }
            else if (_this.args.type === "Gauge") {
                Gauge.resize.call(_this);
            }
        });
    }
}

export default {extendDefaults, wrap, initializeEvents}