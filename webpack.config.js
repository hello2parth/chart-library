var path = require("path");
var CleanWebpackPlugin = require("clean-webpack-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin"); // Require  to generate html file
var MiniCssExtractPlugin = require("mini-css-extract-plugin"); // Require  to generate css file


module.exports = {
  entry: {
    'chart.min': path.join(__dirname, "/src/app/index.js"),
    'main': path.join(__dirname, "/src/public/main.js"),
  },
  output: {
    path: path.join(__dirname, "dist"), // Folder to store generated bundle
    filename: '[name].js', // Name of generated bundle after build
    library: ["Chart"],
    libraryTarget: "umd"
  },
  mode: "production",
  module: {  // where we defined file patterns and their loaders
      rules: [
          {
            test: /\.js$/,
            use: "babel-loader",
            exclude: [
              /node_modules/
            ]
          },
          {
            test: /\.css$/,
            use: [ "style-loader", "css-loader" ]
          }
          // ,
          // {
          //   test: /\.css$/,
          //   use: [
          //     MiniCssExtractPlugin.loader,
          //     "css-loader"
          //   ],
          //   exclude: [
          //     /node_modules/
          //   ]
          // }
      ]
  },
  // optimization: {
  //   splitChunks: {
  //     cacheGroups: {
  //       styles: {
  //         name: "chart",
  //         test: /\.css$/,
  //         chunks: "all",
  //         enforce: true
  //       }
  //     }
  //   }
  // },
  plugins: [  // Array of plugins to apply to build chunk
      new CleanWebpackPlugin(["dist"],{
        root:     __dirname,  // Absolute path to your webpack root folder
        verbose:  true // Write logs to console.
      }),
      new HtmlWebpackPlugin({
          template: __dirname + "/src/public/index.html",
          inject: "body"
      })
      // ,
      // new MiniCssExtractPlugin({
      //   filename: "[name].css"
      // })
  ],
  devServer: {  // configuration for webpack-dev-server
      contentBase: "./src/public",  //source of static assets
      port: 8080, // port to run dev-server
      compress: true, // enable gzip compression
      historyApiFallback: true, // true for index.html upon 404, object for multiple paths
      host: "0.0.0.0"
  }
};
